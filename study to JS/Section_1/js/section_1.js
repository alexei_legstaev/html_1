// Типы данных (Видео 6)

// JS - динамически типизированный язык

// Примитивные типы данных

/**
 * -Number: 2022, 3.14, NaN - Not a Number, Infinity.
 * -String: 'Hello', "Hello", `Hello`.
 * -Boolean: true, false.
 * -Null: null.
 * -Undefined: undefined
 * -Symbol ()
 */

let value;

//Number

value = 2022;
console.log(value);
console.log(typeof value);

value = 20 * 'lol';
console.log(value);
console.log(typeof value);

value = 1/0;
console.log(value);
console.log(typeof value);

console.clear();

//String

console.log("hello", 'Hello', `HELLO`);

console.clear();

//Boolean

value = true;
console.log(value);
console.log(typeof value);

value = false;
console.log(value);
console.log(typeof value);

console.clear();

//Null

value = null;

console.log(value);
console.log(typeof value);

console.clear();

//Undefined

let value_undefined;//пустая переменная
console.log(value_undefined);
console.log(typeof value_undefined);

console.clear();

//Symbol

console.clear();

// Ссылочный тип - когда записываем в переменную храниться ссылка в памяти на этот объект. Когда переприсваеваем то
// будет присваиваться непосредственно ссылка, а не само значение.

// Объект состоит из пары ключ - идентификатор. Набор свойств и методов.
// Свойства - ключ содержащий любое значение String, Number, Object и тд
// Методы - ключи которые содержат в себе функции
// Подтипы объектов - массивы, функции, даты, сложные данные.

console.log({name: 'Olexii', age: 28 });

console.table ({name: 'Olexii', age: 28 });

//Массив
console.log([1, 2, 3, 4, 5]);

console.clear();

// Переменные (Видео 7)

//var, let, const

// Пробелы после переменной и равно. Также после var перед названием переменной
var name = 'Olexii'; // Объявление переменной.
console.log(name);
name = 'Alex';       // Мы имеем возможность переназначать переменную. Переопределение переменной.
console.log(name);

var age; // Объявление переменной.
console.log(age);
console.log(typeof age); // Будет undefined. Из-за отсутствующего значения переменной.
age = 27; //Переопределение переменной.
console.log(age);

// Проблемы var

var car = 'bmw';
var car = 'audi'; // Может быть не одна переменная с тем де именем.

console.log(car); // Выведеться крайнее значение.

// есть эффект всплытия
console.clear();

let nickName = 'Last Centurion';
//let nickName = 'LC'; Переменной let не может быть несколько.
nickName = 'LC'; //Можно переназначать.
console.log(nickName);

// нет эффекта всплытия.

console.clear();

const firstName = 'Olexii';
//firstName = 'Alex'; не может быть переназначена.
//const firstName; Нельзя создавать пустой;

console.log(firstName);
//const мутабельны
const user = {
    name: 'Olexii',
    age: 30
};
user.age = 28; // Содержимое массива может изменяться. Но не значение самой const.
console.log(user);
console.clear();

// Преобразование типов (Видео 8)

//let value; Задана выше.

//Number to string
value = String(10); // специальная функция String которая преобразует в строку любое значение.
value = String(10 + 40);
value = (40).toString(); // специальный метод, делает преобразования над данными которые находятся слева.

// Boolean to string
value = String(true);
value = String(false);

// Array to string
value = String([1, 2, 3]);

//Object to string
value = String ({name: 'Olexii'}); //получим [object Object] - нюансы JS
                                        // (возвращает значение по умолчанию, унаследованное от Object)

//Конкатенация = сложение

value = 30 + '' + 30; //Неявное преобразование в строку. ('' - пустой символ (если есть пробел то это уже не пусто))
value = 30 - 'LC' //Получим NaN ибо 'LC' не может быть преобразовано к числу.
value = 30 - '5' // получим 25 (typeof number), это будет при любых мат дейсвиях
                // но если value = 30 + '5', то из-за конкатенации получим 305 (typeof string)

// String to number
value = Number('23'); //получим 23 (typeof number)
value = Number(true); //получим 1 (typeof number)
value = Number(false); //получим 0 (typeof number)
value = Number(null); //получим 0 (typeof number)
value = Number('true'); //получим NaN (typeof number)
value = Number([1, 2, 3]); //получим NaN (typeof number)

// (ДЛЯ ЦЕЛЫХ ЧИСЕЛ)Функция parseInt() принимает строку в качестве аргумента
// и возвращает целое число в соответствии с указанным основанием системы счисления.
value = parseInt('200') //получим 200 (typeof number)
value = parseInt('200asdasdasdasd') //получим 200 (typeof number)
value = parseInt('asdasdasdasd200asdasdasdasd') //получим NaN (typeof number)

// (ДЛЯ ДОБНЫХ ЧИСЕЛ)Функция parseFloat() принимает строку в качестве аргумента
// и возвращает десятичное число (число с плавающей точкой)
value = parseInt('200.002') //получим 200.002 (typeof number)
value = parseInt('200.002asdasdasdasd') //получим 200.002 (typeof number)
value = parseInt('asdasdasdasd200.002asdasdasdasd') //получим NaN (typeof number)

// Bolean

value = Boolean('hello'); // любая не пустая строка даже ' ', будет true
value = Boolean (''); // будет false
value = Boolean (0); // будет false, все остальные числа true
value = Boolean (null); // будет false
value = Boolean (undefined); // будет false
value = Boolean ([]); // будет ture
value = Boolean ({}); // будет ture

console.log(value);
console.log(typeof value);

console.clear()

// Числа (Видео 9)

const num1 = 10;
const num2 = 20;
let result;

//+ * / -
result = num1 + num2;
result = result + 100; // арифметические операции имеют более высокий приоритет,
                        // сначала выполняются действия которые, находятся справа, а потом происходит присвоение
result += 100; //  result = result + 100; то же но короче result += 100;
result = 4 % 2; // остаток от деления
result = num1;

// операторы инкремент и декремент
result++; // Изменения происходят на следующей строке. Увеличивает значение на 1 (инкремент)
result--; // Изменения происходят на следующей строке. Уменьшает значение на 1 (декремент)
++result; // Изменения происходят сразу же на этой строке.
--result; // Изменения происходят сразу же на этой строке.

// Не точные вычисления

result = 0.6 + 0.7;
result = parseFloat(result.toFixed(1)); // parseFloat, +result.toFixed(1) для (typeof number)
result = (0.6 *10+ 0.7*10)/10;

// объект Math - набор свойств для работы с числами.
result = Math.Pi;
result = Math.random(); // случайное число от 0 до 1.
result = Math.round(2.4); // нормальное округление.
result = Math.ceil(2.4); // округление вверх до ближайшего целого аргумента
result = Math.floor(2.4); // округление вниз. Округляет аргумент до ближайшего меньшего целого.
result = Math.min(2, 12, 15, 0, 12) // возвращает минимальное значение из списка.
result = Math.floor(Math.random() * 10 + 1) // случайное число от 1 до 10.
const arr = ['black', 'red', 'yellow', 'pink', 'white', 'blue', 'orange', 'green'];
result = Math.floor(Math.random() * arr.length ) // length - длина масcива,
                                                    // +1 не нужен из-за то го что расчет начинаеться с 0
// 'black' = 0, 'red' = 1, 'yellow' = 2, 'pink' = 3, 'white' = 4, 'blue' = 5, 'orange' = 6, 'green' = 7

console.log(result, arr[result]);

console.clear()

// Строки (Видео 10)

const firstName10 = 'Olexii';
const lastName10 = 'Lehostaev';
const age10 = 28;
const str = 'Hello my name is Olexii';

let value10;

value10 = firstName10 + lastName10; // OlexiiLehostaev (вывод без разделителя)
value10 = firstName10 + ' ' + lastName10; // Olexii Lehostaev (вывод с разделителем)

value10 = value10 + ' I am ' + age10; // Olexii Lehostaev I am 28
value10 += ' I am ' + age10; // Olexii Lehostaev I am 28 то же но корче.

value10 = firstName10.length; // Выведется длина строки (5)
value10 = firstName10[2]; // Выведется символ с заданным индексом (2)

// JS создает временные объекты для работы с примитивами (со значениями этой строки и методами и свойств).
// Для того что бы к ним можно было применять методы и функции

value10 = lastName10[lastName10.length -1]; //получаем последний элемент строки
//Применяя методы к строке мы не меняем исходную строку. Методы возвращают измененное значение.

value10 = firstName10.toUpperCase(); // Всю строку в верхний регистр
value10 = firstName10.concat(' ', lastName10); // Конкатенирует строки которые нам надо.

value10 = firstName10.indexOf('i'); // indexOf метод, который принимает подстроку которую мы хотим найти,
                                    // indexOf возвращает индекс первого вхождения, если его не было он возвращает -1

value10 = str.indexOf('l', 10); // Первое значение искомое, второе с места которого искать.

// lastIndexOf метод, который принимает подстроку которую мы хотим найти, но с конца.

value10 = str.includes('my') // Возвращает булевое значение (true/false) в зависимости от результата.

value10 = str.slice(0,5) // Метод slice() возвращает новую строку, содержащую копию части исходной строки.
// Первый символ стартовый, второй конечный, который не включен,
// без указания второго значения вырежет всю строку от начала,
// принимает отрицательные значения, slice(0, -3) вернет всю строку кроме последних трех символов

value10 = str.replace('Olexii', 'Alex') // Метод replace() заменяет выбранное нами значение,
                                                            // на записанное нами

console.log(value10);

console.clear();

// Шаблонные строки (Видео 11)

const firstName11 = 'Olexii';
const lastName11 = 'Lehostaev';
const age11 = '28';

let str11;

str11 = 'Hello my name is ' + firstName11 + ' ' + lastName11;

str11 = '<ul>' +
            '<li>First name: ' + firstName11 + '</li>' +
            '<li>Last name: ' + lastName11 + '</li>' +
            '<li>Age: ' + age11 + '</li>' +
        '</ul>';

//console.log(str11)

//document.body.innerHTML = str11;

// ES6

str11 = `<ul>
            <li>First name: ${ firstName11 } </li>
            <li>Last name: ${ lastName11 } </li>
            <li>Age: ${ age11 } </li>
            <li>Math.random: ${ Math.random() }</li>
            <li>5+5 = ${ 5+5 }</li>
        </ul>`

//document.body.innerHTML = str11;


// Введение в объекты (Видео 12)

const user12 = {
    firstName12: 'Olexii',
    age12: 30,
    isAdmin12: true,
    email12: 'test@test.com',
    'user-address12': { // Кавычки для имени ключа нужны в том случае если без них имя будет не валидным. Либо с мат. знаками
        city: "Zaporizhzhia"
    },
    skills12: ['html', 'css', 'js']
};

let value12;
let prop12 = 'skills12';


value12 = user12.firstName12;
value12 = user12['isAdmin12'];
value12 = user12['user-address12'];
value12 = user12['user-address12'].city; // Выводит из объекта только вложенное свойство которое мы ищем
value12 = user12['user-address12']['city']; // Выводит из объекта только вложенное свойство которое мы ищем
// если писать без кавычек, то значение city будет распознано как переменная и будет ошибка
// так же с использованием квадратных скобок мы можем подставлять переменные
value12 = user12[prop12][0];

//перезапись свойств в объекте

user12.firstName12 = 'Alex'; // Изменили значение firstName12: 'Olexii' на 'Alex'.
user12.info12 = 'Some info'; //  Если мы обращаемся к свойству которого нет в объекте,
// оно создается со значением которое мы присваиваем ему после равно

user12['user-address12'].city = 'Kiev'; // перезапись вложенного объекта
user12['user-address12'].country = 'Ukraine'; // создания нового свойства во вложенном объекте
//user12.plan12.basic12 = 'basic'; // мы не можем добавить в несуществующий вложенный объект.

console.log(value12);
console.log(user12);

console.clear();

// Логические операторы, if statement (Видео 13)

// >, <, >=, <=, ==, ===, !=, !==

let value13;

value13 = 1 > 2; // false
value13 = 1 <=2; //true
value13 = 1 == 1; //true
value13 = 1 == '1'; //true по тому что знак равенства преобразует тип данных в Number
value13 = 1 == true; // true по тому что знак равенства преобразует тип данных в Number
value13 = 1 === '1'; //false по тому что строгое равнество не преобразует данные, а сразу сравнивает
value13 = 1 != '1';   //true по тому что знак неравенства преобразует тип данных в Number
value13 = 1 !== '1'; //false по тому что строгое неравнество не преобразует данные, а сразу сравнивает
value13 = 'a' > 'A'; // true из-за того формата как символы записаны в Unicode
value13 = 'a' > 'ab'; // false строки сравниваются по символьно
value13 = 'a'.charCodeAt(); // .carCodeAt - возвращает числовое значение символа.

console.log(value13);
console.clear();

// {код который выполниться при условиях}

// if (условие) { // условием может быть любой валидный код.
//     //action
// } else {
//     //else actions
// }

/*value13 = 10;

if (value13 === 10) { // лучше делать строгие проверки, что бы не происходило не явных преобразований.
    console.log('value13: 10');
} else {
    console.log('else');
}
console.clear();


value13 = null; //так же будет срабатывать при '', 0, NaN

if (value13) {
    console.log('some action', value13);
} else {
    console.log('else', value13);
}*/

// || - или, && - и, ! - не.

/*value13 = null;

console.log(!value13); //! - возвращает к противоположному булевому значению

if (!value13) {
    console.log(value13);
}*/

/*value13 = [1];

if (value13.length) {
    console.log(value13)
} else {
    console.log('array is empty')
}*/

/*if (Array.isArray(value13)) {
    console.log(value13)
} else {
    console.log('array is empty')
}*/

/*
let user13 = {
    name13: 'Olexii'
};

if (user13.name13) {
    console.log(user13.name13);
} else {
    console.log('else');
}

// Метод hasOwnProperty() возвращает логическое значение, указывающее, содержит ли объект указанное свойство.

if (user13.hasOwnProperty('name13')) {
    console.log(user13.name13);
} else {
    console.log('else');
}*/

// || &&

/*value13 = 1 || 0;
// оператор && запинается (останавливается) на false, оператор && пытается каждое значение преобразовать к false
// оператор || запинается (останавливается) на true оператор || пытается каждое значение преобразовать к true
console.log(value13);

let age13 = 10;

if (age13 < 16 || age13 > 65) {
    console.log('some actions')
} else {
    console.log('else')
}
let serverNickname13 = 'Alex';
let nickName13 = serverNickname13 || 'default nickname'

console.log(nickName13);*/

/*value13 = 1 && 0 && 3; // будет возвращать первую False - в данном случае 0
value13 = 1 && 2 && 3; // будет возвращать первую False, либо последнюю true - в данном случае 3
console.log(value13);

let productPrice13 = 10;

if (productPrice13 >= 5 && productPrice13 <=20) {
    console.log('take');
} else {
    console.log("don't take")
}*/

value13 = 10;

if (value13 < 10) {
    console.log('value13 < 10', value13);
} else if (value13 >= 10) {
    console.log('value13 >= 10', value13);
} else {
    console.log('else');
}
console.clear();

// Тернарный оператор. Конструкция switch case (Видео 14)

let a = 1;
let b = 0;

/*if (a > 0) {
    b = a;
} else {
    b += 1;
}*/

// Выражение ? если true: если false;

//a > 0 ? b = a : b +=1;

let c = a > 0 ? b = a : b +=1;
console.log(`b: ${b}, c: ${c}`);

b = a > 0 ? b = a : b + 1;
console.log(`b: ${b}`);

// Выражение ? если true: Выражение ? если true: если false;

b = a > 0 ? 2: a < 0 ? 3 : 0;

console.log(`b: ${b}`);

console.clear();

//switch.case

let color14 = 'yellow';

switch(color14) {
    case 'yellow':
        console.log('Color is yellow');
        break;
    case 'red':
        console.log('Color is red');
        break
    default:
        console.log('unknown')
}

console.clear();

// Циклы (Видео 15)

//while, do while, for, for of, for in

//итератор условие действие

/*let i = 0;

while(i < 10) {
    console.log(i)
    i++
}*/

/*
while(i++ < 10) {
    console.log(i)
}*/

/* i = 10

while(i--) {        // цикл закончит выполнение в 0. 0 = False.
    console.log(i)
}*/

/*
let i = 10;
do {
    console.log('action')
    i--
} while (i > 0)*/

/*for (let i = 0; i < 10; i++) {
    console.log (i);
}*/

/*let str15 = 'Hello';
let res15 = '';
for (let i = 0; i < str15.length; i++) {
    res15 += str[i] + '*';
}
console.log(res15);*/

/*let colors15 = ['white', 'black', 'yellow', 'orange'];

for (let i = 0; i <colors15.length; i++) {
    console.log(colors15[i]);
}*/


/*for (let i = 0; i < 10; i++) {
    if (i === 5) {
        continue;             // пропускает 1 цикл.
    }
    console.log(i)
}*/



/*for (let i = 0; i < 10; i++) {
    if (i === 5) {
        break;             // останавливает цикл.
    }
    console.log(i)
}*/

/*
const user15 = [
    {
        name15: 'Olexii',
        age15: 28
    },
    {
        name15: 'Oleg',
        age15: 12
    },
    {
        name15: 'Maks',
        age15: 25
    },
    {
        name15: 'Olga',
        age15: 2
    },
];
*/

/* НЕ СРАБАТІВАЕТ
const users15Obj = {};

for (let i = 0; i < user15.length; i++) {
    //console.log(user15[i].name15);
    users15Obj[user15[i].name] = user15[i];
}
console.log(users15Obj);*/

/*const user15 = {
    name15: 'Olexii',
    age15: 28
};*/

/*for (let key in user15) {
    console.log(key);
    console.log(user15[key]);
}*/

/*for (let value15 of user15) {
    console.log(value15);
}*/

// введение в функции (Видео 16)

//function Decorator

/*
function sayHello(firstName16 = "Default", lastName16= "Default") {
    if (!firstName16) return console.error("Error");
    console.log(firstName16, lastName16);
    console.log("Hello World");
    return `Hello ${firstName16} ${lastName16}`;
}
//обладают эффектом всплывания
// let res16 = sayHello("Oleksii", "Lehostaiev");
// let res162 = sayHello("Oleksii", "Lehostaiev") + "!";
let res163 = sayHello()
console.log(res163);
*/
//Область видимости функции

// let x16 = 10;
// Если объявить переменную внтури функции, то она будет локальной переменной самой функции, если этого не делать, то
//заменит собой глобальные значения такой же переменной.
/*
function foo16() {
    x16 = 20;
    console.log(x16);
}
foo16();
console.log(x16);

const user16 = {
    name16: 'Oleksii',
    age16: 30
};

function getObj16(obj) {
    console.log(obj);
    obj.name16 = "Alex";
}

getObj16(user16);
console.log(user16);
console.clear();

//function expression

const square16 = function (x) {
    return x * x
}*/
