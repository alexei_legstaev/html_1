//1. Получить число pi из Math и округлить его до 2-х знаков после точки

/*let Pi;

Pi = Math.PI;

Pi = Pi.toFixed(2)

console.log(Pi);*/

//2. Используя Math, найти максимальное и минимальное числа из представленного ряда 15, 11, 16, 12, 51, 12, 13, 51

/*let maxNumb;

maxNumb = Math.max(5, 11, 16, 12, 51, 12, 13, 51);

console.log(maxNumb)*/

/*3. Работа с Math.random:
 a. Получить случайное число и округлить его до двух цифр после запятой
 b. Получить случайное целое число от 0 до X. X - любое произвольное число. */

/*let randomNumb;

randomNumb = Math.random()

randomNumb = randomNumb.toFixed(2)

console.log(randomNumb);

let randomNumbX;

randomNumbX = Math.round(Math.random() * 17);

console.log(randomNumbX);*/

//Проверить результат вычисления 0.6 + 0.7 - как привести к нормальному виду (1.3)

/*let numb;

numb = (0.6 * 10 + 0.7 * 10) / 10;

console.log(numb);

numb = 0.6 + 0.7;

numb = numb.toFixed(1)

console.log(numb);*/

//Получить число из строки ‘100$’

/*let value;

value = '100$';

value = parseInt(value)

console.log(value);

console.log(typeof value);*/

//console.clear();

/*---Задание 2: Домашнее задание на строки--------------------------------------------*/

//let string = 'some test string';
//ВРУЧНУЮ НИЧЕГО НЕ СЧИТАТЬ

//1. Получить первую и последнюю буквы строки
/*let string = "some test string";

console.log(string.charAt(0));
console.log(string[string.length - 1]);*/

//2. Сделать первую и последнюю буквы в верхнем регистре

/*console.log("some test string"[0].toUpperCase());
console.log("some test string"[string.length - 1].toUpperCase());*/

//3. Найти положение слова ‘string’ в строке

/*console.log(string.indexOf('string'));*/

//4. Найти положение второго пробела (“вручную” ничего не считать)

/*console.log(string.lastIndexOf(" "));*/

//5. Получить строку с 5-го символа длиной 4 буквы

/*console.log(string.substr(5, 4));*/

//6. Получить строку с 5-го по 9-й символы

/*console.log(string.slice(5, 9));*/

//7. Получить новую строку из исходной путем удаления последних 6-и символов
//   (то есть исходная строка без последних 6и символов)

/*console.log(string.substring(0, 9));*/


//8. Из двух переменных a=20 и b=16 получить переменную string, в которой будет
//   содержаться текст “2016”

/*let a;
let b;
let c;
a = "20";
b = "16";
c = a + b;
console.log(c);
console.log(typeof c)

console.clear();*/

/*---Задание 3: Домашнее задание на объекты--------------------------------------------*/

// Все поля добавлять по очереди, не создавать сразу готовый объект со всеми полями.

// 1. Создать объект с полем product, равным ‘iphone’
/*const homework3 = {
    product: 'iphone',
};*/

// 2. Добавить в объект поле price, равное 1000 и поле currency, равное ‘dollar’

/*homework3.price = 1000;
homework3.currency = 'dollar';*/

// 3. Добавить поле details, которое будет содержать объект с полями model и color

/*homework3.details = {};

homework3.details.model = '';
homework3.details.color = '';

console.log(homework3);

console.clear();*/

/*---Задание 4: Основы. Выражения. Конструкция if else--------------------------------------------*/

// 1. Если переменная равна “hidden”, присвоить ей значение “visible”, иначе - “hidden”.

/*let value4;

value4 = 'hidden';

if (value4 === 'hidden') {
    value4 = 'visible';
    console.log(value4);
} else {
    value4 = 'hidden';
    console.log(value4);
}
console.clear();*/

// 2. Используя if, записать условие:

// если переменная равна нулю, присвоить ей 1;
// если меньше нуля - строку “less then zero”;
// если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).

/*value4 = 0;

if (value4 === 0) {
    value4 = 1;
    console.log(value4);
} else if (value4 < 0) {
    value4 = "less then zero";
    console.log(value4);
} else if (value4 > 0) {
    value4 *= 10;
    console.log(value4);
} else {
    console.log('else');
}*/

// 3. Дан объект let car = { name: 'Lexus', age: 10, create: 2008, needRepair: false }.
//     Написать условие если возраст машины больше 5 лет
//     то нужно вывести в консоль сообщение 'Need Repair' и свойство needRepair в объекте car изменить на true;
//     иначе изменить на false.

/*let car = {name: 'Lexus', age: 10, create: 2008, needRepair: false}

if (car.age > 5) {
    console.log('Need Repair');
    car.needRepair = true;
} else {
    car.needRepair = false;
}
console.clear()*/
// 4. Дан объект let item = { name: 'Intel core i7', price: '100$', discount: '15%' }.
//     Написать условие если у item есть поле discount и там есть значение которое не NaN а также есть поле price
//     значение которого также не NaN то в объекте item создать поле priceWithDiscount и записать туда цену с учетом
//     скидки и вывести ее в консоль, обратите внимание что поля discount и price это строки и вам из них нужно получить
//     числа чтобы выполнить расчет. иначе если поля discount нет то вывести просто поле price в консоль.

/*let item = {name: 'Intel core i7', price: '100$', discount: '15%'}


if (!isNaN(parseInt(item?.discount)) && !isNaN(parseInt(item?.price))) {
    item.priceWithDiscount = parseInt(item.price) - parseInt(item.discount) * parseInt(item.price) / 100
    console.log(item.priceWithDiscount)
} else {
    console.log(item.price)
}*/


// 5. Дан следующий код:

//     let product = {
//         name: “Яблоко”,
// price: “10$”
// };
//
// let min = 10; // минимальная цена
// let max = 20; // максимальная цена
//
// Написать условие если цена товара больше или равна минимальной цене и меньше или равна максимальной цене то вывести
// в консоль название этого товара, иначе вывести в консоль что товаров не найдено.

/*let product = {
    name: 'Яблоко',
    price: '10$'
};

let min = 10; // минимальная цена
let max = 20; // максимальная цена

let repairedPrice = parseInt(product.price);

if (parseInt(product.price) >= min && repairedPrice <= max) {
    console.log(product.name)
} else {
    console.log('товар не найдено')
}

console.clear();*/


/*let item1 = {
    name: "Intel core i7",
    price: "100$",
    discount: "15%",
    phones: {
        apple: {
            iphone: {
                x68: "asdasd",
            },
        },
    },
};*/

/*let item2 = {
    test: "bla",
    calc: 9,
};

let bla1 = Object.assign(item1, item2, {name: "Intel core I9"});
let bla2 = {...item1, ...item2, name: "Intel core I9"};

let clone = JSON.parse(JSON.stringify(item1));*/


// DEEP_CLONE;


/*console.log(item?.items?.microsoft?.iphone?.price);


function getCounter() {
    let counter = 0;
    return function () {
        counter = counter + 1;
        return counter;
    }
}

let count = getCounter();

console.log(count())
console.log(count())
console.log(count())
console.log(count())
console.log(count())
console.log(count())
console.log(count())
console.log(count())

console.clear();*/

/*---Задание 5: Домашнее задание по тернарным операторам и switch case--------------------------------------------*/


// 1. Записать в виде switch case следующее условие:
//     if (a === ‘block’) {
//     console.log(‘block’)
// } else if (a === ‘none’) {
//     console.log(‘none’)
// } else if (a === ‘inline’) {
//     console.log(‘inline’)
// } else {
//     console.log(‘other’)
// }
//Записать условие, используя конструктор switch. В консоли должно отразиться только одно значение.

/*let a5;

a5 = 'block';

switch (a5) {
    case 'block':
        console.log('block');
        break;
    case 'none':
        console.log('none');
        break;
    case 'inline':
        console.log('inline');
        break;
    default:
        console.log('other');
}

console.clear();*/


// 2. Из задач по условному оператору if else выполнить задачи 1 и 2 в виде тернарного оператора.
//    1. Если переменная равна “hidden”, присвоить ей значение “visible”, иначе - “hidden”.

/*let b5 = 'hidden'

b5 === 'hidden' ? b5 = 'visible' : b5 = 'hidden';

console.log(`b5 = ${b5}`);

//2. Используя if, записать условие:
//       2.1 если переменная равна нулю, присвоить ей 1;
//       2.2 если меньше нуля - строку “less then zero”;
//       2.3 если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).

let c5 = 0;

c5 === 0 ? c5 = 1 : c5 <= 0 ? console.log('less then zero') : c5 *= 10;

console.log(c5);

console.clear();*/

/*---Задание 6: Домашнее задание по циклам--------------------------------------------*/

// 1. На основе строки “i am in the easycode” сделать новую строку, где первые буквы каждого слова
// будут в верхнем регистре. Использовать for или while.
/*const str7 = 'i am in the easycode';

let str6 = str7.split(' ');

for (let i = 0; i < str6.length; i++) {
    console.log(str6[i]);
    str6[i] = str6[i][0].toUpperCase() + str6[i].slice(1);
}

let str8 = str6.join(' ');
console.log(str8);*/

/*const str6 = 'i am in the easycode'.split(' ').map(el=>el[0].toUpperCase()+el.slice(1)).join(" ")

console.log(str6);*/

// 2. Дана строка “tseb eht ma i”. Используя циклы, сделать строку-перевертыш (то есть последняя буква становится первой,
//     предпоследняя - второй итд).

/*let str61 = 'tseb eht ma i'.split('').reverse().join('');

console.log(str61);*/
/*let str67 = Array(str61.length);
for (let i = 0; i < str61.length%2; i++) {
    let a = str61.length - i;
    let b = i;
    str67[b] = str61[a]
    str67[a] = str61[b]
}
console.log(str67);*/


// 3. Факториал числа - произведение всех натуральных чисел от 1 до n
// включительно: 3! = 3*2*1, 5! = 5*4*3*2*1. С помощью циклов вычислить факториал числа 10. Использовать for.
/*
let num6 = 1;
for (i=0; i < 10; i++) {
    num6 = num6*(i+1);
    console.log(num6);
}
*/

// 4. На основе строки “JavaScript is a pretty good language” сделать новую строку,
//     где каждое слово начинается с большой буквы, а пробелы удалены. Использовать for.

/*
const str64 = 'JavaScript is a pretty good language';

let str641 = str64.split(' ');

for (let i = 0; i < str641.length; i++) {
    str641[i] = str641[i][0].toUpperCase() + str641[i].slice(1);
}

console.log(str641.join(''));
*/

/*const str64 = 'JavaScript is a pretty good language'.split(' ').map(el=>el[0].toUpperCase()+el.slice(1)).join("")

console.log(str64);*/

// 5. Найти все нечетные числа в массиве от 1 до 15 включительно и вывести их в консоль.
// Массив [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] Использовать for of.

/*
let arr6 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
for (let arr61 of arr6) {
    let arr62 = arr61%2
    if (arr62 === 1){
       console.log(arr61);
    };
}
*/


//     6. Дан объект:
//     let list = {
//         name: ‘denis’,
// work: ‘easycode’,
// age: 29
// }
// Перебрать объект и если значение в свойстве это строка то переписать ее всю в верхнем регистре. Использовать for in.


/*let list = {
        name: 'denis',
        work: 'easycode',
        age: 29,
     isAdmin: true
}

for (let key in list) {

    if (typeof list[key] === 'string') {
        list[key] = list[key].toUpperCase();
    }
        console.log(list[key]);
}
console.clear();*/
/*---Задание 7: Домашнее задание функциям--------------------------------------------*/

// 1. Создать функцию multiply, которая будет принимать любое количество чисел и возвращать их произведение:
// multiply(1,2,3) = 6 (1*2*3)
// Если нет ни одного аргумента, вернуть ноль: multiply() // 0

/*
function multiply() {
    let s = 1;
    if(arguments.length===0) return 0;
    for(let i=0; i<arguments.length; i++) s *= arguments[i];
    return s
}

console.log(multiply(1,2,3,4,5,10));
console.clear();
*/

// 2. Создать функцию, которая принимает строку и возвращает строку-перевертыш: reverseString(‘test’) // “tset”.

/*function reverseString(test) {
    if(test === undefined) { test = "undefined"
    } else if(test === null) { test = "null";
    } else if (typeof test === "number") {test = test.toString();
    }
    return  test.split("").reverse().join("");
}
console.log(reverseString(1234));*/
// 3. Создать функцию, которая в качестве аргумента принимает строку из букв и возвращает строку,
// где каждый символ разделен пробелом и заменен на юникод-значение символа:
//     getCodeStringFromText(‘hello’) // “104 101 108 108 111”
// подсказка: для получения кода используйте специальный метод

/*function getCodeStringFromText() {
        //test = prompt('Введите слово', 'Test');

        return
    }*/

/*let test9 = 'alloha';
let test10;

for (let i = 0; i < test9.length; i++){
    test10 = test9.charCodeAt(i);
    console.log(test10);
    Array.push(test10)
    test10.concat(test10)

console.log(typeof test10)}
//*/

/*function getCodeStringFromText(str) {
    if (str === undefined) {
        str = "undefined"
    } else if (str === null) {
        str = "null";
    }

    let arr = [];
    for (let i = 0; i < str.length; i++) {
        let str1 = str.charCodeAt(i);
        arr.push(str1);
    }
    let str2 = arr.join(" ");
    return console.log(str2);
}

getCodeStringFromText("1234");*/

// 4. Создать функцию угадай число. Она принимает число от 1-10
// (обязательно проверить что число не больше 10 и не меньше 0).
// Генерирует рандомное число от 1-10 и сравнивает с заданным числом если они совпали то возвращает “Вы выиграли”
// если нет то “Вы не угадали ваше число 8 а выпало число 5”. Числа в строке указаны как пример вы подставляете
// реальные числа.

/*function guessNumb(numb0 = 0) {
    let numb741 = Math.random()*10
    let numb742 = Math.round(numb741)
    numb0 = prompt('Введите число от 1 до 10', '0');
    numb0 = parseInt(numb0);
    if (numb0 < 0)  {
        alert('Введите число от 1 до 10')
        return guessNumb()
    }else if (numb0 > 10){
        alert('Введите число от 1 до 10')
        return guessNumb()
    } else if (isNaN(numb0)){
        alert('Введите число от 1 до 10')
        return guessNumb()
    }
    return numb742 == numb0 ? console.log('Ви выиграли') : console.log(`Вы не угадали, ваше число ${numb0}, а выпало ${numb742}`);
}
guessNumb()*/

/*
function getRandomNumber(min = 0, max = 10) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function guessNumber() {
    const randomInteger = getRandomNumber();
    const promptedNumber = Number(prompt("Введите число от 1 до 10"));
    const hasError =
        isNaN(promptedNumber) ||
        !promptedNumber ||
        promptedNumber < 0 ||
        promptedNumber > 10;

    if (hasError) throw new Error("Ошибка");

    const messageOnFinish =
        randomInteger === promptedNumber
            ? "Ви выиграли"
            : `Вы не угадали, ваше число ${promptedNumber}, а выпало ${randomInteger}`;

    console.log(messageOnFinish);
}

guessNumber();
*/

/*
function guessTheNumber(num) {
    const number = Number(num);

    if (typeof number !== "number" || isNaN(number)) return new Error("Please provide a valid number");
    if (number < 0 || number > 10) return new Error("Please provide number in range 0 - 10");

    const random = Math.ceil(Math.random() * 10);

    if (random === number) return "You win!";

    return `You are lose, your number is ${number}, the random number is ${random}`;
}

console.log(guessTheNumber(21));*/

// 5. Создать функцию, которая принимает число n и возвращает массив,
// заполненный числами от 1 до n: getArray(10); // [1,2,3,4,5,6,7,8,9,10]
// Данное задание выполните после того как познакомитесь с методами массивов.


/*function getArray(num) {
    let arr = [];
    for (let i = 1; i <= num; i++) {
        arr.push(i);
    }
    return arr;
}
console.log(getArray(22));*/


// 6. Создать функцию, которая принимает массив, а возвращает новый массив с дублированными элементами входного массива.
// Данное задание выполните после того как познакомитесь с методами массивов:
//     doubleArray([1,2,3]) // [1,2,3,1,2,3]
//НЕПРАВИЛЬНО
/*function doubleArray(num) {
    let arr1 = [];
    let arr2 = [];
    let arr3
    for (let i = 1; i <= num; i++) {
        arr1.push(num);
    }
    for (let i = 1; i <= num; i++) {
        arr2.push(num);
    }
    arr3 = arr1.concat(arr2)
    return arr3;
}
console.log(doubleArray(5));*/

/*function doubleArray(num) {
    return num.concat(num);
}
console.log(doubleArray([1,2,3]));*/

/*let doubleArray = (num => num.concat(num));
console.log(doubleArray([1,2,3]));*/

/*function doubleArray(num) {
    let num1 = num.map(item => num.concat(num));
    return console.log(num1) //ВОЗВРАЩАЕТ КОНКАТЕНИРОВАНЫЕ МАССИВЫ (НО В КОЛИЧЕСТВЕ ИХ ЭЛЕМЕНТОВ)
}
doubleArray([1,2,3,4]);*/

// 7. Создать функцию, которая принимает произвольное (любое) число массивов и удаляет из каждого массива первый элемент,
// а возвращает массив из оставшихся значений. Данное задание выполните после того как познакомитесь с методами массивов:
//     changeCollection([1,2,3], [‘a’, ’b’, ‘c’]) → [ [2,3], [‘b’, ‘c’] ], changeCollection([1,2,3]) → [ [2,3] ] и т.д.

/*function changeCollection() {
        if(arguments.length===0) return 0;
        for(let i=0; i<arguments.length; i++) arguments[i].pop();
        return Array.from(arguments)
}
console.log(changeCollection([1,2,3], ['a', 'b', 'c']))*/



// 8. Создать функцию которая принимает массив пользователей, поле на которое хочу проверить и значение на которое хочу
// проверять. Проверять что все аргументы переданы. Возвращать новый массив с пользователями соответсвующие указанным параметрам.
//     Данное задание выполните после того как познакомитесь с методами массивов
// funcGetUsers(users, “gender”, “male”); // [ {name: “Denis”, age: “29”, gender: “male”} ,
// {name: “Ivan”, age: “20”, gender: “male”} ]

/*function funcGetUsers(users, gender, age) {
    users = prompt('Введите имя', '');
    gender = prompt('Введите пол (male/female)', '');
    age = parseInt(prompt('Возраст', ''));

    let newArr = [];
    newArr.push(users, gender, age)
    return console.log(newArr);
}
funcGetUsers();*/


/*const users = [
    {
        "_id": "5e36b779dc76fe3db02adc32",
        "balance": "$1,955.65",
        "picture": "http://placehold.it/32x32",
        "age": 33,
        "name": "Berg Zimmerman",
        "gender": "male"
    },
    {
        "_id": "5e36b779d117774176f90e0b",
        "balance": "$3,776.14",
        "picture": "http://placehold.it/32x32",
        "age": 37,
        "name": "Deann Winters",
        "gender": "female"
    },
    {
        "_id": "5e36b779daf6e455ec54cf45",
        "balance": "$3,424.84",
        "picture": "http://placehold.it/32x32",
        "age": 36,
        "name": "Kari Waters",
        "gender": "female"
    }
]

/!*function filterUsers(users, key, value) {
    const filteredUsers = users.filter((user) => user[key] === value);
    return filteredUsers.length ? filteredUsers : new Error("Error message");
}*!/


function filterUsers(arr, key, value) {
    if (!Array.isArray(arr)) return new Error('The first argument should be an array');
    if (typeof key !== "string" || key === '') return new Error('The key should be a valid string');
    if (value === undefined || value === null) return new Error('The value should be a valid value.');
    return users.filter(user=> user[key] === value);

}
console.log(filterUsers(users, "age", 36))*/

/*---Задание 7: Функции высшего порядка. Callback--------------------------------------------*/

/*1. Создать две функции и дать им осмысленные названия:
    - первая функция принимает массив и колбэк (одна для всех вызовов)
    - вторая функция (колбэк) обрабатывает каждый элемент массива (для каждого вызова свой callback)

Первая функция возвращает строку “New value: ” и результат обработки:

    firstFunc([‘my’, ‘name’, ‘is’, ‘Trinity’], handler1) → “New value: MyNameIsTrinity”
    firstFunc([10, 20, 30], handler2) → “New value: 100, 200, 300,”
    firstFunc([{age: 45, name: ‘Jhon’}, {age: 20, name: ‘Aaron’}], handler3) → “New value: Jhon is 45, Aaron is 20,”
    firstFunc([‘abc’, ‘123’], handler4) → “New value: cba, 321,” // строки инвертируются

Подсказка: secondFunc должна быть представлена функцией, которая принимает
один аргумент (каждый элемент массива) и возвращает результат его обработки*/

/*function firstFunc(arr, fn) {
    return secondFunc(arr, fn)
}
function secondFunc (arr, fn) {
    if (fn === 'handler1') {
       let hl1 = arr.join('')
        return console.log(hl1);
    } else if (fn === 'handler2') {
        let hl2 = arr.map(item => item * 10)
        return console.log(hl2);
    } else if (fn === 'handler3') {
        let c = [];
        let d
        for(let i = 0; i < arr.length; i++) {
            let a = arr[i].name;
            c.push(a);
            c.push(' is ');
            let b = arr[i].age;
            c.push(b);
            c.push(', ');
        }
        c.pop();
        d = c.join('');
        return console.log(d);
    } else if (fn === 'handler4') {
        let hl4
        let hl42 = [];
        for(let i = 0; i < arr.length; i++) {
            hl4 = arr[i];
            let hl41 = hl4.split('').reverse().join('');
            hl42.push(hl41);
        } return console.log(hl42) ;
    }
}

firstFunc(['my', 'name', 'is', 'Trinity'], 'handler1') // “New value: MyNameIsTrinity”
firstFunc([10, 20, 30], 'handler2') // “New value: 100, 200, 300,”
firstFunc([{age: 45, name: 'Jhon'}, {age: 20, name: 'Aaron'}], 'handler3') // “New value: Jhon is 45, Aaron is 20,”
firstFunc(['abc', '123'], 'handler4') // “New value: cba, 321,” // строки инвертируются*/

/*

function firstFunc(arr, fn) {
    let res = 'New value: ';
    for (let i = 0; i < arr.length; i++) {
        res = res + fn(arr[i]);
    }
    return res.trim();
}

function handler1(el) {
    return el.charAt(0).toUpperCase() + el.slice(1);
}

console.log(firstFunc(['my', 'name', 'is', 'Trinity'], handler1)); //MyNameIsTrinity

function handler2(el) {
    return el*10 + ', '
}

console.log(firstFunc([10, 20, 30], handler2) ) // “New value: 100, 200, 300,”

function handler3(el) {
    return el.name + ' is ' + el.age + ', '
}

console.log(firstFunc([{age: 45, name: 'Jhon'}, {age: 20, name: 'Aaron'}], handler3)); // “New value: Jhon is 45, Aaron is 20,”

function handler4(el) {
    return el.split('').reverse().join('') + ' ';
}

console.log(firstFunc(['abs', '123'], handler4)); // “New value: cba, 321,” // строки инвертируются
*/

// 2. Написать аналог метода every. Создайте функцию every, она должна принимать первым аргументом массив чисел
// (обязательно проверьте что передан массив) вторым аргументом callback (обязательно проверьте что передана функция)
// функция должна возвращать true или false в зависимости от результата вызова callback (проверить число больше 5).
// Callback должен принимать один элемент массива, его индекс в массиве и весь массив.

// РЕШЕНИЕ ОТ АВТОРА
/*function every(arr, fn) {
    if (!Array.isArray(arr)) return new Error('The first argument expected as array');
    if (!fn || typeof fn !== 'function') return new Error('The second argument expected as function');

    for (let i = 0; i < arr.length; i++) {
        if (!fn(arr[i], i, arr)) {
            return false;
        }
    }

    return true;
}

console.log(every([1,2], function (el) {
    return typeof el === 'number';
}))*/

/*function every(arr, fn) {
    let res
    let newArr = []
    if (!Array.isArray(arr) || typeof fn !== 'function' || !fn) {
        return new Error('ne to.')
    }
    for (let i = 0; i < arr.length; i++) {
        res = newArr.push((fn(arr[i], i, arr)))
    }
    let i = 0;
    while (i < newArr.length) {
        if (newArr[i] === false) break
        i++;
        if (newArr[i] === true) {
            return true
        }
    }
    if (newArr[i] === false){
        return false;
    }
}

function callbackEvery (currentValue, i, arr) {
    //console.log(currentValue);
    if (currentValue > 5) {
        return true;
    } else if (currentValue !== 'number'|| currentValue <= 5) {
        return false;
    }
}

console.log(every([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], callbackEvery));*/

/*function every(arr, fn) {

    if (!Array.isArray(arr) || !fn || typeof fn !== 'function' ) {
        return new Error('ne to.')
    }
    for (let i = 0; i < arr.length; i++) {
       if (!fn(arr[i], i, arr)) {
           return false
       }
    }
    return true
}

function callbackEvery (currentValue, i, arr) {
    return currentValue === 'number' || currentValue > 5;
}

console.log(every([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], callbackEvery));*/
